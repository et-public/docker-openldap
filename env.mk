# Docker configuration
DOCKER_IMAGE=openldap
DOCKER_IMAGE_VERSION=2.4.59-bullseye

# The AR regisitory repository
DOCKER_REGISTRY=us-docker.pkg.dev
DOCKER_REPO=${GCP_PROJECT_ID}/docker-private
DOCKER_REGISTRY_PASSWORD_PATH=${SEC_PATH}/common/ar-write

# GitLab configuration
GITLAB_SERVER=https://code.stanford.edu
GITLAB_REPO=authnz/docker-${DOCKER_IMAGE}
GITLAB_SEC_FILE=.gitlab-ci.sec
SLACK_WEBHOOK_PATH=${SEC_PATH}/common/slack/gitlab-integration
SLACK_GITLAB_CHANNEL=authnz-git-commits
SLACK_CICD_CHANNEL=authnz-build
