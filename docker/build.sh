#!/bin/bash -e
# Maintainer: sfeng@stanford.edu

OPENLDAP_VERSION=openldap-2.4.59
CYRUS_SASL_VERSION=cyrus-sasl-2.1.27
OPENLDAP_TARBALL=ftp://ftp.openldap.org/pub/OpenLDAP/openldap-release/${OPENLDAP_VERSION}.tgz
CYRUS_SASL_TARBALL=https://github.com/cyrusimap/cyrus-sasl/releases/download/${CYRUS_SASL_VERSION}/${CYRUS_SASL_VERSION}.tar.gz
DEBIAN_FRONTEND=noninteractive
WORKDIR=/
BUILDDIR=/build

# Download software required to build OpenLAP and Cyrus SASL
function apt_get_install {
    apt-get update && \
    apt-get install -y -qq  \
    ca-certificates \
    coreutils \
    file \
    gcc \
    groff-base \
    libsasl2-2 \
    libc6-dev \
    libkrb5-dev \
    libssl-dev \
    libsasl2-dev \
    libperl-dev \
    libltdl-dev \
    libltdl7 \
    libkrb5-3 \
    libgssapi-krb5-2 \
    krb5-user \
    kstart \
    lmdb-utils \
    libtcmalloc-minimal4 \
    libsasl2-modules=2.1.27* \
    libsasl2-modules-gssapi-mit \
    openssl=1.1.1* \
    make \
    patch \
    procps \
    wget
}

# Download packages
function download_packages {
    wget ${CYRUS_SASL_TARBALL} 1> NUL 2> NUL
    tar xzvf ${CYRUS_SASL_VERSION}.tar.gz
    wget ${OPENLDAP_TARBALL} 1> NUL 2> NUL
    tar xzvf ${OPENLDAP_VERSION}.tgz
}

# Insatall Cyrus SASL
function install_cyrus_sasl {
    cd ${WORKDIR}/${CYRUS_SASL_VERSION}
    sed -i'' '/maj_stat\s=\sgss_acquire_cred/{n;s/text->server_name/GSS_C_NO_NAME/}' plugins/gssapi.c
    ./configure --prefix=/usr --sysconfdir=/etc --libexecdir='${prefix}/lib' \
        --enable-digest=no \
        --enable-gssapi \
        --with-gss_impl=mit \
        CFLAGS="-Wl,-z,defs -Wl,--no-as-needed -Wno-cast-function-type -Wno-implicit-function-declaration"
    make
    make install
} 

# Install OpenLDAP
function install_openldap {
    cd ${WORKDIR}/${OPENLDAP_VERSION}
    sed -i '/SUBDIRS/s/doc//' Makefile.in
    # echo "patch -p1 /its9295.patch"
    # ls -l /its9295.patch
    # patch -p1 < /its9295.patch
    ./configure \
        --prefix=/usr \
        --libexecdir='${prefix}/lib' \
        --sysconfdir=/etc \
        --localstatedir=/var \
        --mandir='${prefix}/share/man' \
        --disable-hdb \
        --disable-bdb \
        --disable-slp \
        --disable-ndb \
        --disable-sql \
        --disable-lmpasswd \
        --enable-debug \
        --enable-dynamic \
        --enable-syslog \
        --enable-proctitle \
        --enable-local \
        --enable-slapd \
        --enable-dynacl \
        --enable-aci \
        --enable-cleartext \
        --enable-crypt \
        --enable-spasswd \
        --enable-modules \
        --enable-rewrite \
        --enable-rlookups \
        --enable-slapi \
        --enable-backends=mod \
        --enable-overlays=mod \
        --with-subdir=ldap \
        --with-cyrus-sasl \
        --with-threads \
        --with-tls=openssl \
        LDFLAGS="-L/usr/lib/sasl2 -Wl,-rpath,/usr/lib/sasl2" \
        CFLAGS="-Wno-cast-function-type \
                -Wno-implicit-function-declaration \
                -Wno-incompatible-pointer-types \
                -Wno-pointer-compare"
    make depend && make 
    make install
    # Clean up default configurations
    rm -rf /etc/ldap/*.default \
        /etc/ldap/*.conf \
        /etc/ldap/slapd.ldif
}

function clean_up {
   apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /tmp/download
}

## MAIN
cd ${WORKDIR}
apt_get_install
download_packages
install_cyrus_sasl
install_openldap
clean_up
