[[_TOC_]]

[![pipeline status](https://code.stanford.edu/authnz/docker-openldap/badges/master/pipeline.svg)](https://code.stanford.edu/authnz/docker-openldap/-/commits/master)

# Stanford OpenLDAP in Container

This repository contains instructions on how to build and publish docker-openldap image to
Google Container Registry.
# Update OpenLdap image

To upgrade software version and base image, edit build.sh, env.sh to change `OPENLDAP_VERSION`, and edit `.gitlab.yml` to change tag.

If you make changes to the repository, you can test build manually. If the build is successful,
then commit and push changes to the docker-openldap repository. It will trigger CI/CD to build and upload
gcr.io/uit-authnz/openldap:latest image.

## Manual push to Google container registry

```console
# make build
$ make push

Updating PS cloud framework from Git into /Users/sfeng/bin/ps-cloud-framework...
VAULT SERVER: https://vault.stanford.edu
You are logged in VAULT and have permission to read from secret/projects/uit-authnz/*
Login Succeeded
Push gcr.io/uit-authnz/ldap:048d8295ebbd129086db9161b12b101eb685959e ...
The push refers to repository [gcr.io/uit-authnz/ldap]
a8eb0a957605: Pushed
188e99cd395e: Pushed
f819153fc68b: Pushed
d982fc324e3e: Pushed
4276ef46cf43: Pushed
96e57cd3c993: Layer already exists
240b0bdf7f50: Layer already exists
202b974c712a: Layer already exists
43a852aaa685: Layer already exists
latest: digest: sha256:e56efd42b560cffa77a588b0e9ecde71110ec74a6c101d7f53a838a587a460a1 size: 2402
```
