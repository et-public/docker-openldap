# Terraform version
TF_VERSION = 1.2.7

# GCLOUD Configuration
GOOGLE_CLOUD_PROJECT=uit-authnz
GCP_PROJECT_ID=${GOOGLE_CLOUD_PROJECT}
GCP_PROJECT_NAME=${GOOGLE_CLOUD_PROJECT}
GCP_CONFIGURATION=${GCP_PROJECT_NAME}-${GCP_ENVIRONMENT}
GCP_REGION=us-west1
GCP_ZONE=${GCP_REGION}-a
GCP_ENVIRONMENT=default
GCP_DNS_DOMAIN=iam.stanford.edu
ACME_DNS_PROVIDER=${GCP_PROJECT_NAME}-d
GCP_NETWORK=services

# Default SSL policy
MIN_TLS_VERSION=TLS_1_2
SSL_POLICY_PROFILE=MODERN
SSL_POLICY_NAME=${MIN_TLS_VERSION}-${SSL_POLICY_PROFILE}

# Default Docker registry
DOCKER_NAMESPACE=${GCP_PROJECT_ID}
DOCKER_REGISTRY=gcr.io

# Force gcloud auth with user credentials
GCP_USER_AUTH=true

# Google group that are granted permissions to GCP resources (iam.tf)
GCP_WORKGROUP=authnz_ops@stanford.edu

# Required by Terraform: APPLICATION_DEFAULT_CREDENTIALS
GCP_INFRASTRUCTURE_BUCKET=${GCP_PROJECT_ID}-infrastructure
TF_BACKEND_PREFIX=terraform/${GCP_PROJECT_ID}/${GCP_ENVIRONMENT}/state

# PS Cloud Framework (Scripts, shared config, etc.)
FRAMEWORK_DIR=${HOME}/bin/ps-cloud-framework
FRAMEWORK_BUCKET=ps-cloud-framework
SCRIPTS_DIR=${FRAMEWORK_DIR}/scripts

# Vault and secrets configuration
VAULT_ADDR=https://vault.stanford.edu
VAULT_AUTH_METHOD=ldap
VAULT_CACHE=${HOME}/.vault-local
SEC_PATH=secret/projects/${GCP_PROJECT_NAME}
GCP_KEY_PATH=${SEC_PATH}/common/gcp-provision
GCP_KEY_FILE=${VAULT_CACHE}/${GCP_KEY_PATH}
EXTERNAL_DNS_GCP_CREDENTIALS_PATH=${SEC_PATH}/common/dns-admin-key
EXTERNAL_DNS_DOMAIN_FILTERS=iam.stanford.edu
DOCKER_REGISTRY_PASSWORD_PATH_GCR_USER=${SEC_PATH}/common/gcr-user
DOCKER_REGISTRY_PASSWORD_PATH_GCR_PULL=${SEC_PATH}/common/gcr-pull
SPLUNK_ADDON_SA=${SEC_PATH}/common/splunk-addon-sa

# GitLab ci configuration
GITLAB_SERVER=https://code.stanford.edu
GITLAB_SEC_FILE=../.gitlab-ci.sec
SLACK_WEBHOOK_PATH=${SEC_PATH}/common/slack/gitlab-integration
SLACK_GITLAB_CHANNEL=authnz-git-commits
SLACK_CICD_CHANNEL=authnz-build






# Sub-projects dir
SUB_PROJECTS=sub-projects

# GKE Configuration
GKE_CLUSTER_NAME=${GCP_ENVIRONMENT}-${GCP_REGION}
KUBE_CONTEXT=gke_${GCP_PROJECT_ID}

# set kube config default namespace
KUBE_NAMESPACE=${APP_NAMESPACE}

# reserved cidrs for gke masters,  /28 CIDR blocks
GKE_MASTER_CIDR_PROD=172.16.0.16/28
GKE_MASTER_CIDR_STAGE=172.16.0.32/28
GKE_MASTER_CIDR_DEV=172.16.0.48/28

# reserved cidrs for firestore,  /29 CIDR blocks
FS_CIDR_PROD=172.16.1.8/29
FS_CIDR_STAGE=172.16.1.16/29
FS_CIDR_DEV=172.16.1.32/29
FS_TIER=STANDARD
# capacity in number of TB
FS_CAPACITY=1
FS_NAME=filestore-${GCP_ENVIRONMENT}

# Other applications need to know the backup-monitor-user name and email
BACKUP_MONITOR_USER=backup-monitor-user
BACKUP_MONITOR_USER_EMAIL=${BACKUP_MONITOR_USER}@${GCP_PROJECT_NAME}.iam.gserviceaccount.com

#########
# Storage buckets created and used in gke-cluster for each environment; put in here so kube-ldap can share the env.

# ldap backup bucket
LDAP_BACKUP_BUCKET=${GCP_ENVIRONMENT}-${GCP_PROJECT_NAME}-ldap-backup
LDAP_BACKUP_BUCKET_LOCATION=US
FORCE_DESTROY_LDAP_BACKUP_BUCKET=true

# General data bucket (for idp, kdc, ldap etc.)
DATA_BUCKET=${GCP_ENVIRONMENT}-${GCP_PROJECT_NAME}-data
DATA_BUCKET_LOCATION=US
FORCE_DESTROY_DATA_BUCKET=true

# General public data bucket (for idp, kdc, ldap etc.)
DATA_PUBLIC_BUCKET=${GCP_ENVIRONMENT}-${GCP_PROJECT_NAME}-data-public
DATA_PUBLIC_BUCKET_LOCATION=US
FORCE_DESTROY_DATA_PUBLIC_BUCKET=true

# KDC backup bucket
KDC_BACKUP_BUCKET=${GCP_ENVIRONMENT}-${GCP_PROJECT_NAME}-kdc-backup
KDC_BACKUP_BUCKET_LOCATION=US
FORCE_DESTROY_KDC_BACKUP_BUCKET=true
KDC_NUMBER_NEWER_VERSIONS_BACKUP_BUCKET=30

# WALLET backup bucket
WALLET_BACKUP_BUCKET=${GCP_ENVIRONMENT}-${GCP_PROJECT_NAME}-wallet-backup
WALLET_BACKUP_BUCKET_LOCATION=US
FORCE_DESTROY_WALLET_BACKUP_BUCKET=true
WALLET_NUMBER_NEWER_VERSIONS_BACKUP_BUCKET=30

 GITLAB_REPO=authnz/uit-authnz
